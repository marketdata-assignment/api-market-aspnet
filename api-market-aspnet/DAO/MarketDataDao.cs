﻿using api_marker_aspnet.Model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace api_marker_aspnet.DAO
{
    public class MarketDataDao
    {
        private MySqlConnection connection;
        private MySqlCommand command;


        public MarketDataDao()
        {
        }

        private MySqlConnection GetConnection()
        {
            if (connection == null)
            {
                return new MySqlConnection("server=localhost;database=market;uid=ALEXANDRE;pwd=password;");
            }
            return connection;
        }

        /// <summary>
        /// this methode get all market data by brand.
        /// </summary>
        /// <param name="brand">brand of the market data</param>
        /// <returns>list of market data</returns>
        public List<MarketData> getAllFrom(String brand)
        {
            List<MarketData> marketDataList = new List<MarketData>();

            connection = GetConnection();
            connection.Open();

            String query = "select * from MARKET.stock_prices where UPPER(brand) = '" + brand.ToUpper() + "' order by date_stock desc";

            command = new MySqlCommand(query, connection);
            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    marketDataList.Add(new MarketData(
                        reader.GetString("brand"),
                        reader.GetDateTime("date_stock"),
                        reader.GetFloat("open_price"),
                        reader.GetFloat("high"),
                        reader.GetFloat("low"),
                        reader.GetFloat("close_price")
                    ));

                }
            }

            connection.Close();
            return marketDataList;
        }

        /// <summary>
        /// this add a market data.
        /// </summary>
        /// <param name="md">the market data</param>
        public void add(MarketData md)
        {
            connection = GetConnection();
            connection.Open();

            String query = "insert into market.stock_prices(brand, date_stock, open_price, high, low, close_price) values " +
                "('" + md._brand + "', " +
                "'" + String.Format("{0:yyyy-MM-dd HH:mm:ss}", md._date) + "', " +
                md._open.ToString().Replace(",", ".") + ", " +
                md._high.ToString().Replace(",", ".") + ", " +
                md._low.ToString().Replace(",", ".") + ", " +
                md._close.ToString().Replace(",", ".") + ");";

            command = new MySqlCommand(query, connection);
            command.ExecuteNonQuery();

            connection.Close();
        }

        /// <summary>
        /// this methode delete all market data from a brand.
        /// </summary>
        /// <param name="brand">brand of the market data</param>
        public void delete(String brand)
        {
            connection = GetConnection();
            connection.Open();

            String query = "delete FROM market.stock_prices where brand = '"+brand+"';";
            command = new MySqlCommand(query, connection);
            command.ExecuteNonQuery();

            connection.Close();
        }
    }
}