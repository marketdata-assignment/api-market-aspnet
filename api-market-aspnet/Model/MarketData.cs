﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace api_marker_aspnet.Model
{
    public class MarketData
    {
        // Brand of MarketData
        public String _brand { get; set; }

        // Date of MarketData
        public DateTime _date { get; set; }

        // Open price of MarketData
        public Double _open { get; set; }

        // High price of MarketData
        public Double _high { get; set; }

        // Low price of MarketData
        public Double _low { get; set; }

        // Close price of MarketData
        public Double _close { get; set; }

        // Constructor
        public MarketData()
        {
        }

        // Constructor with params
        public MarketData(String brand, DateTime date, Double open, Double high,
                          Double low, Double close)
        {
            this._brand = brand;
            this._date = date;
            this._open = open;
            this._high = high;
            this._low = low;
            this._close = close;
        }
    }
}