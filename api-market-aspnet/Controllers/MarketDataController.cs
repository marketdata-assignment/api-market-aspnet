﻿using api_marker_aspnet.DAO;
using api_marker_aspnet.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace api_marker_aspnet.Controllers
{
    public class MarketDataController : ApiController
    {
        private MarketDataDao marketDataDao;

        // GET api/<controller>
        public IEnumerable<Object> Get()
        {
            marketDataDao = new MarketDataDao();
            List<Object> result = new List<object>();
            List<MarketData> firstMarket = marketDataDao.getAllFrom("AAPL");
            List<MarketData> secondMarket = marketDataDao.getAllFrom("S&P500");

            result.Add(firstMarket);
            result.Add(secondMarket);
            return result;
        }

        // GET api/<controller>
        public Object Get(String calcul)
        {
            marketDataDao = new MarketDataDao();
            List<Object> result = new List<object>();
            List<MarketData> firstMarket = marketDataDao.getAllFrom("AAPL");
            List<MarketData> secondMarket = marketDataDao.getAllFrom("S&P500");

            result.Add(firstMarket);
            result.Add(secondMarket);
            return result;
        }

        // POST api/<controller>
        public void Post(List<MarketData> jsonObject)
        {
            String brand = jsonObject[0]._brand;

            marketDataDao = new MarketDataDao();
            marketDataDao.delete(brand);

            foreach (MarketData md in jsonObject) {
                marketDataDao.add(md);
            }
        }
    }
}