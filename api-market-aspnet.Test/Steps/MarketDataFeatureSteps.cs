﻿using api_marker_aspnet.DAO;
using api_marker_aspnet.Model;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace api_market_aspnet.Test.Steps
{
    [Binding]
    [TestFixture]
    public class MarketDataFeatureSteps
    {
        [Given(@"I have MarketDataDao")]
        public void GivenIHaveMarketDataDao()
        {
            var marketDataDao = new MarketDataDao();
            ScenarioContext.Current["MarketDataDao"] = marketDataDao;
        }

        [When(@"I delete market data from AAPL")]
        public void WhenIDeleteMarketDataFromAAPL()
        {
            var dao = ScenarioContext.Current["MarketDataDao"] as MarketDataDao;
            dao.delete("AAPL");
        }


        [When(@"I add the market data")]
        public void WhenIAddTheMarketData(Table table)
        {
            var dao = ScenarioContext.Current["MarketDataDao"] as MarketDataDao;

            String brand = table.Rows[0][0];
            DateTime date = DateTime.Parse(table.Rows[0][1]);
            Double open = Double.Parse(table.Rows[0][2]);
            Double high = Double.Parse(table.Rows[0][3]);
            Double low = Double.Parse(table.Rows[0][4]);
            Double close = Double.Parse(table.Rows[0][5]);

            MarketData md = new MarketData(brand, date, open, high, low, close);
            dao.add(md);
        }

        [Then(@"last market data of AAPL is")]
        public void ThenLastMarketDataOfAAPLIs(Table table)
        {
            var dao = ScenarioContext.Current["MarketDataDao"] as MarketDataDao;
            List<MarketData> list = dao.getAllFrom("AAPL");

            Assert.AreEqual(table.Rows[0][0], list[0]._brand);
            Assert.AreEqual(DateTime.Parse(table.Rows[0][1]), DateTime.Parse(list[0]._date.ToString()));
            Assert.AreEqual(Double.Parse(table.Rows[0][2]), list[0]._open);
            Assert.AreEqual(Double.Parse(table.Rows[0][3]), list[0]._high);
            Assert.AreEqual(Double.Parse(table.Rows[0][4]), list[0]._low);
            Assert.AreEqual(Double.Parse(table.Rows[0][5]), list[0]._close);
        }

        [When(@"I add the market datas")]
        public void WhenIAddTheMarketDatas(Table table)
        {
            var dao = ScenarioContext.Current["MarketDataDao"] as MarketDataDao;

            String brand;
            DateTime date;
            Double open, high, low, close;

            foreach (var row in table.Rows) {
                brand = row[0];
                date = DateTime.Parse(row[1]);
                open = Double.Parse(row[2]);
                high = Double.Parse(row[3]);
                low = Double.Parse(row[4]);
                close = Double.Parse(row[5]);

                MarketData md = new MarketData(brand, date, open, high, low, close);
                dao.add(md);
            }
        }


        [Then(@"I have (.*) rows with brand AAPL")]
        public void ThenIHaveRowsWithBrandAAPL(int p0)
        {
            var dao = ScenarioContext.Current["MarketDataDao"] as MarketDataDao;
            List<MarketData> list = dao.getAllFrom("AAPL");
            Assert.AreEqual(p0, list.Count);
        }
    }
}
