﻿Feature: MarketDataFeature
	In order to avoid silly mistakes
	I want to be check some market data metho

@mytag
Scenario: Add a market data value
	Given I have MarketDataDao
	When I delete market data from AAPL
	When I add the market data
	| brand | date_stock		  | open_price | high | low | close_price |
	| AAPL  | 2018-03-07 00:00:00 | 90		   | 100  | 80  | 95          |
	Then last market data of AAPL is
	| brand | date_stock		  | open_price | high | low | close_price |
	| AAPL  | 2018-03-07 00:00:00 | 90		   | 100  | 80  | 95          |


Scenario: Count market data value from AAPL
	Given I have MarketDataDao
	When I delete market data from AAPL
	When I add the market datas
	| brand | date_stock		  | open_price | high | low | close_price |
	| AAPL  | 2018-03-07 00:00:00 | 90		   | 100  | 80  | 95          |
	| AAPL  | 2018-03-06 00:00:00 | 90		   | 100  | 80  | 95          |
	| AAPL  | 2018-03-05 00:00:00 | 90		   | 100  | 80  | 95          |
	| AAPL  | 2018-03-04 00:00:00 | 90		   | 100  | 80  | 95          |
	Then I have 4 rows with brand AAPL
